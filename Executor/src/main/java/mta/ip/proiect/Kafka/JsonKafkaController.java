package mta.ip.proiect.Kafka;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import mta.ip.proiect.Kafka.KafkaProducer;
@RestController
@RequestMapping("/api/v1/kafka")
public class JsonKafkaController {
    private KafkaProducer kafkaProducer;
    static public KafkaMessage kafkaMessage;
    public JsonKafkaController(KafkaProducer kafkaProducer) {
        this.kafkaProducer = kafkaProducer;
    }

    @PostMapping("/fromExecutor")
    public ResponseEntity<String> sendMessageExecutor() {
        kafkaProducer.sendMessageExecutor(kafkaMessage);
        return ResponseEntity.ok( kafkaMessage.toString());
    }
}