package mta.ip.proiect;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.async.ResultCallback;
import com.github.dockerjava.api.command.BuildImageResultCallback;
import com.github.dockerjava.api.command.CreateContainerResponse;
import com.github.dockerjava.api.model.Frame;
import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.DockerClientConfig;
import com.github.dockerjava.core.command.LogContainerResultCallback;
import com.github.dockerjava.httpclient5.ApacheDockerHttpClient;
import mta.ip.proiect.Kafka.JsonKafkaController;
import mta.ip.proiect.Kafka.KafkaMessage;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

@SpringBootApplication
public class CExecutorApplication {
    private static final String IMAGE_NAME = "my-image";
    private static final String DOCKERFILE_PATH = "C:\\Users\\denis\\OneDrive\\Desktop\\infrastructura\\docker\\src\\main\\resources\\Dockerfile-C\\Dockerfile";
    private static String EXECUTABLE_NAME = "/home/andrei/Desktop/docker/src/main/resources/Cod-C/hello.c";
    private static String TESTS_NAME_ARCHIVE;
    private static String INPUTS_TESTS;
    private static String OUTPUT_TESTS;
    private static String unzip(String zipFilePath, String destDir) {
        File dir = new File(destDir);
        // create output directory if it doesn't exist
        if(!dir.exists()) dir.mkdirs();
        FileInputStream fis;
        String path = new String();
        //buffer for read and write data to file
        byte[] buffer = new byte[1024];
        try {
            fis = new FileInputStream(zipFilePath);
            ZipInputStream zis = new ZipInputStream(fis);
            ZipEntry ze = zis.getNextEntry();
            while(ze != null){
                String fileName = ze.getName();
                File newFile = new File(destDir + File.separator + fileName);
                System.out.println("Unzipping to "+ newFile.getAbsolutePath());
                path = newFile.getAbsolutePath();
                //create directories for sub directories in zip
                new File(newFile.getParent()).mkdirs();
                FileOutputStream fos = new FileOutputStream(newFile);
                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }
                fos.close();
                //close this ZipEntry
                zis.closeEntry();
                ze = zis.getNextEntry();
                return newFile.getAbsolutePath();
            }
            //close last ZipEntry
            zis.closeEntry();
            zis.close();
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return path;
    }

    public static void getStudentUpload() throws IOException {
        String endpoint = "http://localhost:8080/api/v1/kafka/toExecutor";
        HttpURLConnection connection2 = (HttpURLConnection) new URL(endpoint).openConnection();
        connection2.setDoInput(true);
        connection2.setDoOutput(true);
        connection2.setRequestMethod("POST");
        BufferedReader in = new BufferedReader(new InputStreamReader(connection2.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        // Print the response
        System.out.println(response.toString());
        // read the .json file

        String resp = response.toString();
        resp = resp.substring(1, resp.length() - 1);

        System.out.println(resp);
        String[] keyVals = resp.split(", ");

        String username = new String();
        String email = new String();
        String nota = new String();
        String date = new String();
        String idTeacherPost = new String();
        for (String keyVal : keyVals) {
            String[] parts = keyVal.split(":");

            String key = parts[0].substring(1, parts[0].length() - 1);
            String value = parts[1].substring(1, parts[1].length() - 1);
            System.out.println(parts[0] + " "+  parts[1]);

            if(key.equals("note"))
                nota = value;
            else if(key.equals("teacherPost"))
                idTeacherPost = value;
            else if(key.equals("username"))
                username = value.substring(0, value.length() - 1);
            else if(key.equals("email"))
                email = value;
            else if(key.equals("data"))
                date = "C:\\Users\\denis\\OneDrive\\Desktop\\infrastructura\\docker\\src\\main\\resources\\Dockerfile-C\\" + value;
            System.out.println(key + " " + value);

        }
        String zipFilePath = date;
        int index = zipFilePath.lastIndexOf("\\");
        String destDir = zipFilePath.substring(0,index + 1);
        System.out.println(date);

        String execName = unzip(zipFilePath, destDir);
        System.out.println(execName);
        EXECUTABLE_NAME = execName;

        JsonKafkaController.kafkaMessage = new KafkaMessage(resp);
        JsonKafkaController.kafkaMessage.setNota(Integer.parseInt(nota));
        JsonKafkaController.kafkaMessage.setData(date);
        JsonKafkaController.kafkaMessage.setEmail(email);
        JsonKafkaController.kafkaMessage.setId(Integer.parseInt(idTeacherPost));
        JsonKafkaController.kafkaMessage.setUsername(username);
    }

    public static void getProfessorUpload() throws IOException {
        String endpoint = "http://localhost:8080/api/v1/kafka/teacherExecutor";
        HttpURLConnection connection2 = (HttpURLConnection) new URL(endpoint).openConnection();
        connection2.setDoInput(true);
        connection2.setDoOutput(true);
        connection2.setRequestMethod("POST");
        BufferedReader in = new BufferedReader(new InputStreamReader(connection2.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        // Print the response
        System.out.println(response.toString());
        // read the .json file

        String resp = response.toString();
        resp = resp.substring(1, resp.length() - 1);

        String[] keyVals = resp.split(", ");

        String username = new String();
        String email = new String();
        String nota = new String();
        String date = new String();
        String idTeacherPost = new String();
        for (String keyVal : keyVals) {
            String[] parts = keyVal.split(":");

            String key = parts[0].substring(1, parts[0].length() - 1);
            String value = parts[1].substring(1, parts[1].length() - 1);

            if(key.equals("note"))
                nota = value;
            else if(key.equals("teacherPost"))
                idTeacherPost = value;
            else if(key.equals("username"))
                username = value.substring(0, value.length() - 1);
            else if(key.equals("email"))
                email = value;
            else if(key.equals("data"))
                date = "C:\\Users\\denis\\OneDrive\\Desktop\\infrastructura\\docker\\src\\main\\resources\\Dockerfile-C\\" + value;
            System.out.println(key + ":" + value);
        }
        String zipFilePath = date;
        int index = zipFilePath.lastIndexOf("\\");
        String destDir = zipFilePath.substring(0,index + 1);
        System.out.println(destDir);



        TESTS_NAME_ARCHIVE = zipFilePath;
        try (ZipFile file = new ZipFile(zipFilePath))
        {
            Enumeration<? extends ZipEntry> zipEntries = file.entries();
            while (zipEntries.hasMoreElements())
            {
                ZipEntry zipEntry = zipEntries.nextElement();
                File newFile = new File(destDir, zipEntry.getName());

                //create sub directories
                newFile.getParentFile().mkdirs();

                if (!zipEntry.isDirectory())
                {
                    try (FileOutputStream outputStream = new FileOutputStream(newFile))
                    {
                        BufferedInputStream inputStream = new BufferedInputStream(file.getInputStream(zipEntry));
                        while (inputStream.available() > 0)
                        {
                            outputStream.write(inputStream.read());
                        }
                        inputStream.close();
                    }
                }

            }
        }
        System.out.println(destDir);
        INPUTS_TESTS = destDir + "tests\\inputs";
        OUTPUT_TESTS = destDir + "tests\\outputs";
        System.out.println(TESTS_NAME_ARCHIVE + " " + INPUTS_TESTS + " " + OUTPUT_TESTS);
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(CExecutorApplication.class, args);
//        String endpoint = "http://localhost:8080/api/v1/kafka/toExecutor";
//
//        HttpURLConnection connection = (HttpURLConnection) new URL(endpoint).openConnection();
//        connection.setDoInput(true);
//        connection.setDoOutput(true);
//        connection.setRequestMethod("POST");
//        BufferedReader in1 = new BufferedReader(new InputStreamReader(connection.getInputStream()));
//        String inputLine1;
//        StringBuilder response1 = new StringBuilder();
//        while ((inputLine1 = in1.readLine()) != null) {
//            response1.append(inputLine1);
//        }
//        in1.close();
//        System.out.println(response1);

        getProfessorUpload();
        getStudentUpload();

        Integer score = 0;
        Integer test_number = 0;
        for(Integer i = 1; ; i++)
        {
            String input_file = INPUTS_TESTS + "\\" + "in" + i.toString() + ".txt";
            String output_file = OUTPUT_TESTS + "\\" + "out" + i.toString() + ".txt";
            System.out.println(input_file);
            System.out.println(output_file);
            Path inPath = Paths.get(input_file);
            Path outPath = Paths.get(output_file);

            String input;
            String output;
            try
            {
                input = Files.readAllLines(inPath).toString();
                output = Files.readAllLines(outPath).toString();
            }
            catch (IOException e) {
                break;
            }

            System.out.println("a trecut de readuri");
            System.out.println("input: " + input);
            System.out.println("output: " + output);

            DockerClientConfig config = DefaultDockerClientConfig
                    .createDefaultConfigBuilder()
                    .build();
            System.out.println("Config created!");
            DockerClient dockerClient = DockerClientBuilder
                    .getInstance(config)
                    .withDockerHttpClient(new ApacheDockerHttpClient
                            .Builder().
                            dockerHost(config
                                    .getDockerHost())
                            .build())
                    .build();
            System.out.println(EXECUTABLE_NAME);
            EXECUTABLE_NAME = "hello.c";
            dockerClient.buildImageCmd()
                    .withDockerfile(new File(DOCKERFILE_PATH))
                    .withTag(IMAGE_NAME)
                    .withBuildArg("code_path", EXECUTABLE_NAME)
                    .withBuildArg("input", input)
                    //.withBuildArg("entrypoint_path", "/home/andrei/Desktop/docker/src/main/resources/Cod-C/entrypoint.sh")
                    .exec(new BuildImageResultCallback())
                    .awaitImageId();

            CreateContainerResponse container = dockerClient.createContainerCmd(IMAGE_NAME)
                    .withName("c-executor")
                    .exec();

            dockerClient.startContainerCmd(container.getId()).exec();
            dockerClient.waitContainerCmd(container.getId()).exec(new ResultCallback.Adapter<>());

            StringBuilder outputBuilder = new StringBuilder();
            dockerClient.logContainerCmd(container.getId())
                    .withStdOut(true)
                    .withStdErr(true)
                    .exec(new LogContainerResultCallback() {
                        @Override
                        public void onNext(Frame item) {
                            outputBuilder.append(new String(item.getPayload()));
                        }
                    }).awaitCompletion();

            String container_output = outputBuilder.toString();
            System.out.println("Container output:");
            System.out.println(container_output);

            dockerClient.removeContainerCmd(container.getId()).exec();

            test_number++;
            if (output.contains(container_output))
                score++;
        }
        System.out.println("exited for");

        Integer punctaj = 100 * score / test_number;


        System.out.println("Score: " + punctaj.toString());


        JsonKafkaController.kafkaMessage.setNota(punctaj.intValue());



    }
}

