import * as React from 'react';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import { Button, CircularProgress, Divider, Modal } from '@mui/material';
import axios from "axios";
import { baseURL } from '../App';
import { useNavigate } from 'react-router-dom';
import { useState } from "react"
import { ModalAdaugaTema } from './ModalAdaugaTema';
import { ModalPredaTema } from './ModalPredaTema';

interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
}

function TabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`vertical-tabpanel-${index}`}
            aria-labelledby={`vertical-tab-${index}`}
            {...other}
            style={{ minWidth: "70vw" }}
        >
            {value === index && (
                <Box sx={{ p: 3 }}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

export const VerticalTabs = (props: any) => {
    const navigate = useNavigate()

    const [value, setValue] = React.useState(0);
    const [materii, setMaterii] = React.useState<any>();
    const [teme, setTeme] = React.useState<any>();
    const [initializat, setInitializat] = React.useState<boolean>(false);
    const [open, setOpen] = React.useState<boolean>(false);
    const [openPreda, setOpenPreda] = React.useState<boolean>(false);

    const [tipClasa, setTipClasa] = useState("")
    const [subiectID, setSubiectID] = useState("")
    const [subiect, setSubiect] = useState("")

    const handleOpenModal = () => {
        setSubiect(materii[value].idSubject)
        setTipClasa(materii[value].classTypeName)

        setOpen(true)
    }

    const handleOpenModalStudent = () => {
        setSubiectID(materii[value].idSubject)

        setOpenPreda(true)
    }


    const handleChange = (event: React.SyntheticEvent, newValue: number) => {
        setValue(newValue);
        var my_token = props.userToken

        if (props.tipCont === "profesor")
            axios.get(baseURL + "/professor/assignmentsGivenForSubject",
                { params: { idProfessor: 2, idSubject: materii[newValue].idSubject }, headers: { 'Authorization': my_token, 'Access-Control-Allow-Origin': '*', 'Accept': '*/*' }, withCredentials: true })
                .then((response) => {
                    setTeme(response.data)
                })

        if (props.tipCont === "student")
            axios.get(baseURL + "/student/assignmentsforsubject",
                { params: { idStudent: 1, idSubject: materii[newValue].idSubject }, headers: { 'Authorization': my_token, 'Access-Control-Allow-Origin': '*', "Accept": "*/*" }, withCredentials: true })
                .then((response) => {
                    setTeme(response.data)
                })
    };


    React.useEffect(() => {
        if (initializat === false) {
            var my_token = props.userToken

            console.log(props.userToken)
            if (props.tipCont === "profesor")
                axios.get(baseURL + "/professor/classesTaught", { params: { idProfessor: 2 }, headers: { 'Authorization': my_token, 'Access-Control-Allow-Origin': '*', 'Accept': '*/*' }, withCredentials: true }).then((response) => {
                    setMaterii(response.data)
                    axios.get(baseURL + "/professor/assignmentsGivenForSubject",
                        { params: { idProfessor: 2, idSubject: response.data[0].idSubject }, headers: { 'Authorization': my_token, 'Access-Control-Allow-Origin': '*', "Accept": "*/*" }, withCredentials: true })
                        .then((response) => {
                            setTeme(response.data)
                            setInitializat(true)
                        })
                })

            if (props.tipCont === "student")
                axios.get(baseURL + "/student/classesattended", { params: { idStudent: 1 }, headers: { 'Authorization': my_token, 'Access-Control-Allow-Origin': '*', "Accept": "*/*" }, withCredentials: true }).then((response) => {
                    setMaterii(response.data)
                    axios.get(baseURL + "/student/assignmentsforsubject",
                        { params: { idStudent: 1, idSubject: response.data[0].idSubject }, headers: { 'Authorization': my_token, 'Access-Control-Allow-Origin': '*', "Accept": "*/*" }, withCredentials: true })
                        .then((response) => {
                            setTeme(response.data)
                            setInitializat(true)
                        })
                })
        }

    }, [])

    const navigateToResults = (index: number) => {
        navigate("/profesor/istoric", { state: { idMaterie: index, numeMaterie: materii[value].subjectName } })
    }

    const navigateToIstoric = (index: number) => {
        navigate("/student/istoric", { state: { idMaterie: index, numeMaterie: materii[value].subjectName } })
    }

    return (
        <Box
            sx={{ flexGrow: 1, bgcolor: 'background.paper', display: 'flex', height: "100%" }}
        >
            <Tabs
                orientation="vertical"
                variant="scrollable"
                value={value}
                onChange={handleChange}
                sx={{ borderRight: 1, borderColor: 'divider' }}
            >
                {
                    materii !== undefined ?
                        materii.map((materie: any) => (
                            <Tab key={materie.idSubject} label={materie.subjectName + " - " + materie.classTypeName} />
                        ))
                        :
                        <CircularProgress />
                }

            </Tabs>

            {
                materii !== undefined ?
                    materii.map((materie: any, index: number) => (
                        <TabPanel value={value} index={index}>
                            {teme !== undefined ?
                                teme.length > 0 ?
                                    teme.map((tema: any) => (
                                        <div key={tema.postName}>
                                            <h2>{tema.postName}</h2>
                                            {props.tipCont === "student" ?
                                                <div className='proiect-holder'>
                                                    <Button variant="contained" onClick={handleOpenModalStudent} >Incarca</Button>
                                                    <Button variant="contained" onClick={() => navigateToIstoric(materii[value].idSubject)}>Istoric</Button>
                                                </div>
                                                :
                                                <div className='proiect-holder'>
                                                    <Button variant="contained" onClick={() => navigateToResults(materii[value].idSubject)}>Rezultate</Button>
                                                    <Button variant="contained">Sterge proiect</Button>
                                                </div>
                                            }
                                            <Divider style={{ marginTop: "1.5rem" }} />
                                        </div>
                                    ))
                                    :
                                    <h2>Nu a fost postata nicio tema</h2>
                                :
                                <div style={{ textAlign: "center" }}>
                                    <CircularProgress />
                                </div>

                            }
                            <div style={{ marginTop: "2rem" }}>{props.tipCont === "profesor" ?
                                <Button variant="contained" onClick={handleOpenModal}>Adauga tema</Button>
                                :
                                <></>}</div>
                        </TabPanel>
                    ))
                    :
                    <div style={{ textAlign: "center" }}>
                        <CircularProgress />
                    </div>
            }
            <ModalAdaugaTema open={open} setOpen={setOpen} subiect={subiect} classTypeName={tipClasa} />
            <ModalPredaTema open={openPreda} setOpen={setOpenPreda} subiectID={subiectID} />
        </Box>
    );
}