import { Box, Button, Modal, Stack, TextField, Typography } from "@mui/material"
import { useState } from "react";
import dayjs, { Dayjs } from 'dayjs';
import { DateTimePicker } from '@mui/x-date-pickers/DateTimePicker';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import axios from "axios";
import { baseURL } from "../App";
import { DropzoneArea } from "material-ui-dropzone";
import { createStyles, makeStyles } from "@material-ui/core";

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};


export const ModalPredaTema = (props: any) => {

    var newFile = new File(["foo"], "foo.txt", { type: "text/plain", })

    const [filePath, setFilePath] = useState(newFile)

    const handleClose = () => {
        props.setOpen(false)
    }

    const useStyles = makeStyles(theme => createStyles({
        previewChip: {
            minWidth: 100,
            maxWidth: 160,
            color: "white"
        },
    }));

    const classes = useStyles();

    const handlePredaTema = async () => {

        var my_token = props.userToken

        await axios.post(baseURL + '/student/addStudentUpload', null,
            {
                params:
                {
                    idStudent: 1,
                    idTeacherPost: props.subiectID,
                    sourceCodeFilePath: "C:/Users/denis/OneDrive/Desktop/" + filePath.name
                },
                headers: { 'Authorization': my_token, 'Access-Control-Allow-Origin': '*', 'Accept': '*/*' }, withCredentials: true
            }
        )
            .then(() =>
                handleClose()
            )
            .catch(err => {
                handleClose()
            })
    }

    return <>
        <Modal
            open={props.open}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={style}>
                <Typography id="modal-modal-title" variant="h6" component="h2">
                    Preda Tema
                </Typography>

                <Stack spacing={3} style={{ marginTop: "0.8rem" }}>

                    <DropzoneArea
                        showPreviews={true}
                        showPreviewsInDropzone={false}
                        onChange={(files) => setFilePath(files[0])}
                        useChipsForPreview
                        previewGridProps={{ container: { spacing: 0, direction: 'row' } }}
                        previewChipProps={{ classes: { root: classes.previewChip } }}
                    />

                    <Button variant="contained" onClick={handlePredaTema}>Upload tema</Button>
                </Stack>

            </Box>
        </Modal>

    </>
}