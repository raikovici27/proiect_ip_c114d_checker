import { Box, Button, Modal, Stack, TextField, Typography } from "@mui/material"
import { useState } from "react";
import dayjs, { Dayjs } from 'dayjs';
import { DateTimePicker } from '@mui/x-date-pickers/DateTimePicker';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import axios from "axios";
import { baseURL } from "../App";
import { DropzoneArea } from "material-ui-dropzone";
import { createStyles, makeStyles } from "@material-ui/core";

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};


export const ModalAdaugaTema = (props: any) => {

    var newFile = new File(["foo"], "foo.txt", { type: "text/plain", })

    const [postName, setPostName] = useState("")
    const [description, setDescription] = useState("")
    const [filePath, setFilePath] = useState(newFile)


    const [valueStart, setValueStart] = useState<Dayjs | null>(
        dayjs('2023-01-18T21:11:54'),
    );

    const handleChangeStart = (newValue: Dayjs | null) => {
        setValueStart(newValue);
    };

    const [valueEnd, setValueEnd] = useState<Dayjs | null>(
        dayjs('2023-01-18T21:11:54'),
    );

    const handleChangeEnd = (newValue: Dayjs | null) => {
        setValueEnd(newValue);
    };

    const handleClose = () => {
        props.setOpen(false)
    }

    const useStyles = makeStyles(theme => createStyles({
        previewChip: {
            minWidth: 100,
            maxWidth: 160,
            color: "white"
        },
    }));

    const classes = useStyles();

    const handleAdaugaTema = async () => {

        var my_token = props.userToken
        var timeStart = valueStart?.toString()
        var timeEnd = valueEnd?.toString()

        await axios.post(baseURL + '/professor/addTeacherPost', null,
            {
                params:
                {
                    idSubject: props.subiect,
                    idTeacher: 2,
                    description: description,
                    assignmentFilePath: filePath.name,
                    startTime: timeStart,
                    endTimes: timeEnd,
                    postName: postName
                },
                headers: { 'Authorization': my_token, 'Access-Control-Allow-Origin': '*', 'Accept': '*/*' }, withCredentials: true
            }
        )
            .then(() =>
                handleClose()
            )
            .catch(err => {
                handleClose()
            })
    }

    return <>
        <Modal
            open={props.open}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={style}>
                <Typography id="modal-modal-title" variant="h6" component="h2">
                    Adauga Tema
                </Typography>

                <Stack spacing={3} style={{ marginTop: "0.8rem" }}>

                    <TextField
                        id="outlined-password-input"
                        label="Denumirea temei"
                        type="text"
                        value={postName}
                        onChange={(e) => setPostName(e.target.value)}
                        style={{ width: "100%", margin: "auto", marginTop: "0.9rem" }}
                    />

                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                        <DateTimePicker
                            label="Selectati Timpul de inceput"
                            value={valueStart}
                            onChange={(newValue) => { handleChangeStart(newValue) }}
                            renderInput={(params) => <TextField {...params} />}
                        />

                        <DateTimePicker
                            label="Selectati Timpul de incheiere a temei"
                            value={valueEnd}
                            onChange={(newValue) => { handleChangeEnd(newValue) }}
                            renderInput={(params) => <TextField {...params} />}
                        />
                    </LocalizationProvider>

                    <TextField
                        id="outlined-multiline-static"
                        label="Descriere tema"
                        multiline
                        rows={3}
                        onChange={(e) => setDescription(e.target.value)}
                        defaultValue="Adugati descerierea temei" />

                    <DropzoneArea
                        showPreviews={true}
                        showPreviewsInDropzone={false}
                        onChange={(files) => setFilePath(files[0])}
                        useChipsForPreview
                        previewGridProps={{ container: { spacing: 0, direction: 'row' } }}
                        previewChipProps={{ classes: { root: classes.previewChip } }}
                    />

                    <Button variant="contained" onClick={handleAdaugaTema}>Adauga tema</Button>
                </Stack>

            </Box>
        </Modal>

    </>
}