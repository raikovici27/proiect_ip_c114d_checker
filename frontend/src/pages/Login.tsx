import { Button, FormControl, InputLabel, MenuItem, Paper, Select, SelectChangeEvent, TextField } from "@mui/material"
import axios from "axios"
import { useState } from "react"
import { useNavigate } from "react-router-dom"
import { baseURL } from "../App"

export const Login = () => {

    const navigation = useNavigate()

    const [tipCont, setTipCont] = useState("")
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")

    const login = () => {
        axios.request({
            url: "/auth/realms/SpringBootKeycloak/protocol/openid-connect/token",
            method: "POST",
            baseURL: "http://keycloak.d.checker.apps.fsisc.ro/",
            data: {
                grant_type: "password",
                client_id: "login-app",
                username: username,
                password: password,
            },
            headers: {
                "Content-type": "application/x-www-form-urlencoded",
                "Accept": "*/*"
            },
            withCredentials: true
        }).then(respose => {
            var my_token = "Bearer " + respose.data.access_token

            axios.request({
                url: "http://localhost:8080/login",
                method: "get",
                headers: {
                    'Authorization': my_token,
                    "Accept": "*/*",
                    'Access-Control-Allow-Origin': '*'
                },
                withCredentials: true,
            }).then(respose => {
                localStorage.setItem('tipCont', JSON.stringify(tipCont))
                localStorage.setItem('username', JSON.stringify(username))
                localStorage.setItem('userID', JSON.stringify(respose.data))
                localStorage.setItem('userToken', my_token)

                if (tipCont === "student") {

                    navigation("/student/proiecte")
                }
                else {
                    if (tipCont === "profesor") {
                        navigation("/profesor/proiecte")
                    }
                    else {
                        if (tipCont === "admin") {
                            navigation("/admin/detalii-cluster")
                        }
                    }
                }
            })
        });
    }

    const handleChange = (event: SelectChangeEvent) => {
        setTipCont(event.target.value);
    };

    return (
        <>
            <div className="login-form" >
                <Paper elevation={6} style={{ display: "flex", flexDirection: "column" }}>

                    <h2>Checker Log in</h2>

                    <TextField
                        id="outlined-password-input"
                        label="Email"
                        type="email"
                        value={username}
                        onChange={(e) => setUsername(e.target.value)}
                        style={{ width: "65%", margin: "auto", marginTop: "1.5rem" }}
                    />

                    <TextField
                        id="outlined-password-input"
                        label="Password"
                        type="password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        style={{ width: "65%", margin: "auto", marginTop: "1.5rem" }}
                    />

                    <FormControl style={{ minWidth: "65%", maxWidth: "65%", margin: "auto", marginTop: "1.5rem" }}>
                        <InputLabel id="demo-simple-select-label">Select user</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={tipCont}
                            label="Select user"
                            onChange={handleChange}
                        >
                            <MenuItem value={"student"}>Student</MenuItem>
                            <MenuItem value={"profesor"}>Profesor</MenuItem>
                            <MenuItem value={"admin"}>Admin</MenuItem>
                        </Select>
                    </FormControl>

                    <Button variant="contained" style={{ maxWidth: "80%", margin: "auto", marginTop: "1.5rem", marginBottom: "0.9rem" }}
                        onClick={login}>
                        Log in
                    </Button>

                </Paper>
            </div>

        </>
    )
}