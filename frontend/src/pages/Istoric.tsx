import { Button, CircularProgress } from '@mui/material';
import Box from '@mui/material/Box';
import { DataGrid, GridColDef } from '@mui/x-data-grid';
import axios from 'axios';
import React, { useState } from 'react';
import { useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import { baseURL } from '../App';


const columns: GridColDef[] = [

    {
        field: 'surname',
        headerName: 'Nume student',
        width: 250,
        sortable: false
    },
    {
        field: 'firstname',
        headerName: 'Prenume student',
        width: 250,
        sortable: false
    },
    {
        field: 'groupName',
        headerName: 'Grupa',
        width: 150,
        sortable: false
    },
    {
        field: 'result',
        headerName: 'Punctaj',
        width: 110,
    },
    {
        field: 'numeMaterie',
        headerName: 'Nume Materie',
        width: 200,
    },
    {
        field: 'postName',
        headerName: 'Titlu Tema',
        width: 200,
    },
];


export const Istoric = (props: any) => {
    const [initializat, setInitializat] = React.useState<boolean>(false);
    const [rows, setRows] = useState<any>()
    const location = useLocation()

    var my_token = props.userToken

    function delay(ms: number) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    useEffect(() => {
        if (initializat === false) {
            let idMaterie = 1
            let numeMaterie = ""
            idMaterie = location.state.idMaterie
            numeMaterie = location.state.numeMaterie

            axios.get(baseURL + "/general/getResultsForTeacherPost",
                { params: { idTeacherPost: idMaterie }, headers: { 'Authorization': my_token, 'Access-Control-Allow-Origin': '*', 'Accept': '*/*' }, withCredentials: true })
                .then(async (response) => {
                    setInitializat(true)
                    await delay(1500);

                    for (let i = 0; i < response.data.length; i++) {
                        response.data[i].numeMaterie = numeMaterie
                        console.log(response.data[i])
                    }

                    setRows(response.data)
                })
        }
    }, [])


    return (
        <>
            <h2 style={{ textAlign: "center" }}>Istoric proiecte</h2>
            <Box sx={{ height: 500, width: '80%', margin: "auto", marginTop: "2rem" }}>

                {rows !== undefined ?
                    <DataGrid
                        getRowId={(rows) => rows.idStudentUpload}
                        rows={rows}
                        columns={columns}
                        pageSize={5}
                        rowsPerPageOptions={[7]}
                        disableSelectionOnClick
                        experimentalFeatures={{ newEditingApi: true }}
                    />
                    :
                    <div style={{ textAlign: "center" }}>
                        <CircularProgress />
                    </div>

                }

            </Box>

            {props.utilizator === "profesor"
                ?
                <div style={{ textAlign: "center", marginTop: "1.5rem" }}>
                    <Button variant='contained'>Descarca sursele</Button>
                </div>
                :
                <></>
            }
        </>
    )

}

function sleep(arg0: number) {
    throw new Error('Function not implemented.');
}
