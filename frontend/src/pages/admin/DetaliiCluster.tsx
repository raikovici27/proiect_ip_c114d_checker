import { Button, FormControl, InputLabel, MenuItem, Paper, Select, SelectChangeEvent } from "@mui/material"
import { useState } from "react";
import { useNavigate } from "react-router-dom";

export const DetaliiCluster = () => {

    const navigate = useNavigate()

    const [tipExecutor, setTipExecutor] = useState("")
    const [numarExecutori, setNumarExecutori] = useState("")

    const logout = () => {
        localStorage.clear();
        navigate("/")

    }

    const handleChangeExecutor = (event: SelectChangeEvent) => {
        setTipExecutor(event.target.value);
    };

    const handleChangeNumar = (event: SelectChangeEvent) => {
        setNumarExecutori(event.target.value);
    };

    return (
        <div className="login-form" >
            <Paper elevation={6} style={{ display: "flex", flexDirection: "column" }}>

                <h2>Detalii Cluster</h2>

                <FormControl style={{ minWidth: "65%", maxWidth: "65%", margin: "auto", marginTop: "1.5rem" }}>
                    <InputLabel id="demo-simple-select-label">Tipul executorului</InputLabel>
                    <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={tipExecutor}
                        label="Tipul executorului"
                        onChange={handleChangeExecutor}
                    >
                        <MenuItem value={10}>C</MenuItem>
                        <MenuItem value={20}>Java</MenuItem>
                        <MenuItem value={30}>C++</MenuItem>
                    </Select>
                </FormControl>

                <FormControl style={{ minWidth: "65%", maxWidth: "65%", margin: "auto", marginTop: "1.5rem" }}>
                    <InputLabel id="demo-simple-select-label">Numarul executorilor</InputLabel>
                    <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={numarExecutori}
                        label="Numarul executorilor"
                        onChange={handleChangeNumar}
                    >
                        <MenuItem value={10}>1</MenuItem>
                        <MenuItem value={20}>2</MenuItem>
                        <MenuItem value={30}>3</MenuItem>
                        <MenuItem value={30}>4</MenuItem>
                        <MenuItem value={30}>5</MenuItem>
                        <MenuItem value={30}>6</MenuItem>
                    </Select>
                </FormControl>

                <Button variant="contained" style={{ maxWidth: "80%", margin: "auto", marginTop: "1.5rem", marginBottom: "1.2rem" }}>
                    Salveaza
                </Button>

                <Button variant="contained" style={{ maxWidth: "80%", margin: "auto", marginBottom: "0.9rem" }}
                    onClick={logout}>
                    Log out
                </Button>

            </Paper>
        </div>
    )
}