import './App.css';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import { Route, Routes, useLocation } from "react-router-dom";

import { NavBar } from './components/NavBar';
import { Istoric } from './pages/Istoric';
import { VizualizareProiecte } from './pages/profesor/VizualizareProiecte';
import { Login } from './pages/Login';
import { ProiecteleMele } from './pages/student/ProiecteleMele';
import { DetaliiCluster } from './pages/admin/DetaliiCluster';


export const baseURL = "http://localhost:8080"

const darkTheme = createTheme({
  palette: {
    mode: 'dark',
  },
});

export const App = () => {
  const location = useLocation()

  return (
    <ThemeProvider theme={darkTheme}>
      <CssBaseline />

      {location.pathname === '/' || location.pathname === '/admin/detalii-cluster' ? <></> : <NavBar />}


      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/student/istoric" element={<Istoric utilizator={"student"} />} />
        <Route path="/profesor/istoric" element={<Istoric utilizator={"profesor"} />} />
        <Route path="/profesor/proiecte" element={<VizualizareProiecte />} />
        <Route path="/student/proiecte" element={<ProiecteleMele />} />
        <Route path="/admin/detalii-cluster" element={<DetaliiCluster />} />
      </Routes>
    </ThemeProvider>

  );
}
