import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;


public class SendMail {

    public void sendGoogleEmail(String toEmail, String nota) {

        final String fromEmail = "checker.c4d@gmail.com";   // gmail id
        final String password = "qfnifcmrhekneeth";         // password for gmail id

        //definire subiectul si corpul email-lui
        String subject = "Rezultate checker";
        String body = "Punctajul obtinut la tema incarcata pe checker este de :" + nota + " puncte.";

        //System.out.println("Start...");
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");  //SMTP Host
        props.put("mail.smtp.port", "587");             //TLS Port
        props.put("mail.smtp.auth", "true");            //enable authentication
        props.put("mail.smtp.starttls.enable", "true"); //enable STARTTLS

        Authenticator auth = new Authenticator() {
            //override the getPasswordAuthentication method
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(fromEmail, password);
            }
        };
        Session session = Session.getInstance(props, auth);

        try {
            MimeMessage msg = new MimeMessage(session);

            // message headers
            msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
            msg.addHeader("format", "flowed");
            msg.addHeader("Content-Transfer-Encoding", "8bit");

            msg.setSubject(subject, "UTF-8");
            msg.setText(body, "UTF-8");
            msg.setSentDate(new Date());

            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
            System.out.println("Message ready to send...");
            Transport.send(msg);

            System.out.println("Mail Sent Successfully!!!");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}