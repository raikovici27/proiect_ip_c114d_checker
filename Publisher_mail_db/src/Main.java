import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;

public class Main {
    static void MailSend(String mail, String nota){
        SendMail myEmail = new SendMail();
        myEmail.sendGoogleEmail(mail, nota);
    }

    public static void main(String[] args) throws SQLException, IOException {
        System.out.println("Attempting connection");
        publisher_db publisher_database = new publisher_db();
        publisher_database.connect();
        //idStudentUpload   idStudent   idTeacherPost   timestamp   resultsFilePath sourceCodeFilePath
        String endpoint = "http://localhost:8090/api/v1/kafka/fromExecutor";

        HttpURLConnection connection = (HttpURLConnection) new URL(endpoint).openConnection();
        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.setRequestMethod("POST");
        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        // Print the response
        System.out.println(response.toString());
        // read the .json file

        String resp = response.toString();
        resp = resp.substring(1, resp.length() - 1);

        String[] keyVals = resp.split(", ");

        String username = new String();
        String email = new String();
        String nota = new String();
        String date = new String();
        String idTeacherPost = new String();
        for (String keyVal : keyVals) {
            String[] parts = keyVal.split(":");

            String key = parts[0].substring(1, parts[0].length() - 1);
            String value = parts[1].substring(1, parts[1].length() - 1);

            if(key.equals("note"))
                nota = value;
            else if(key.equals("teacherPost"))
                idTeacherPost = value;
            else if(key.equals("username"))
                username = value;
            else if(key.equals("email"))
                email = value;
            else if(key.equals("data"))
                date = value;
            System.out.println(key + " " + value);
        }

        publisher_database.Upload_Student_Result(username,idTeacherPost,"NULL",date,nota);
        MailSend(email, nota);
        System.out.println("Mail sent");
    }
}