package dev.c114d.api.Models;

public class KafkaMessage {
    private int idTeacher;
    private String json;
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getNota() {
        return nota;
    }

    public void setNota(int nota) {
        this.nota = nota;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
    private String username;
    private int nota;
    private String email;
    private String data;

    public int getId() {
        return idTeacher;
    }

    public void setId(int id) {
        this.idTeacher = id;
    }

    public void deserialization() {
        String[] keyVals = json.split(",");
        for(String keyVal:keyVals)
        {
            String[] parts = keyVal.split(":",2);
            String key = parts[0].substring(1, parts[0].length() - 1);
            String value = parts[1].substring(1, parts[1].length() - 1);
            if(key.equals("note"))
                setNota(Integer.parseInt(value));
            else if(key.equals("teacherPost"))
                setId(Integer.parseInt(value));
            else if(key.equals("username"))
                setUsername(value);
            else if(key.equals("email"))
                setEmail(value);
            else if(key.equals("data"))
                setData(value);
        }



    }
    public KafkaMessage(String json) {
        this.json = json;
        this.idTeacher = 0;
        this.data = "";
        this.username = "";
        this.nota = 0;
        this.email = "";
    }

    @Override
    public String toString() {
        return "{" +
                "\"teacherPost\":" + idTeacher + "\"" +
                ", \"username\":\"" + username + "\""+
                ", \"email\":\"" + email + "\"" +
                ", \"note\":\"" + nota + "\"" +
                ", \"data\":\"" + data + "\"" +
                "}";
    }
}


