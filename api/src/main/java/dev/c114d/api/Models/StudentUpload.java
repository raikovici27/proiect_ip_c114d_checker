package dev.c114d.api.Models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.OffsetDateTime;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
public class StudentUpload {

    @Id
    @Column(nullable = false, updatable = false)
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Integer idStudentUpload;

    @Column(nullable = false)
    private OffsetDateTime timestamp;

    @Column(nullable = true, length = 70)
    private String resultsFilePath;

    @Column(nullable = false, length = 70)
    private String sourceCodeFilePath;

    private String result;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idStudent", nullable = false)
    private Students student;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idTeacherPost", nullable = false)
    private TeacherPost teacherPost;

}