package dev.c114d.api.Models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
public class Classes {

    @Id
    @Column(nullable = false, updatable = false)
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Integer idClass;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idSubject", nullable = false)
    private Subjects subject;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idUserTeacher", nullable = false)
    private Users userTeacher;

    @OneToMany(mappedBy = "classs")
    private Set<GroupLists> classsGroupListss;

    @OneToMany(mappedBy = "classs")
    private Set<TeacherPost> classsTeacherPosts;

}

