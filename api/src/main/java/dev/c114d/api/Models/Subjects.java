package dev.c114d.api.Models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
public class Subjects {

    @Id
    @Column(nullable = false, updatable = false)
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Integer idSubject;

    @Column(nullable = false, length = 50)
    private String subjectName;

    @OneToMany(mappedBy = "subject")
    private Set<Classes> subjectClassess;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idClassType", nullable = false)
    private ClassTypes classType;

}

