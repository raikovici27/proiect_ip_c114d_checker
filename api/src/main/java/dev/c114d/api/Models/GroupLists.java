package dev.c114d.api.Models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
public class GroupLists {

    @Id
    @Column(nullable = false, updatable = false)
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Integer idGroupList;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idClass", nullable = false)
    private Classes classs;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idGroup", nullable = false)
    private Groups group;

}

