package dev.c114d.api.Models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
public class Groups {

    @Id
    @Column(nullable = false, updatable = false)
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Integer idGroup;

    @Column(nullable = false, length = 50)
    private String groupName;

    @OneToMany(mappedBy = "group")
    private Set<GroupLists> groupGroupListss;

    @OneToMany(mappedBy = "group")
    private Set<Students> groupStudentss;

}

