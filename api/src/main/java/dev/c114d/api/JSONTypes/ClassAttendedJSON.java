package dev.c114d.api.JSONTypes;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ClassAttendedJSON {
    private Integer idSubject;
    private String subjectName;
    private String classTypeName;
}
