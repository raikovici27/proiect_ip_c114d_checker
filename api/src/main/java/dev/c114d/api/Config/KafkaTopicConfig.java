package dev.c114d.api.Config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class KafkaTopicConfig {

    @Bean
    public NewTopic googleSheetsTopic(){
        return TopicBuilder.name("sheets").partitions(3).build();
    }

    @Bean
    public NewTopic mailTopic(){
        return TopicBuilder.name("mail").partitions(3).build();
    }
    @Bean
    public NewTopic executorTopic(){
        return TopicBuilder.name("executor").partitions(3).build();
    }
}
