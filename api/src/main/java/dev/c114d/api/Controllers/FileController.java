package dev.c114d.api.Controllers;

import dev.c114d.api.FileUploadDownloadLocal.commons.FileResponse;
import dev.c114d.api.FileUploadDownloadLocal.storage.StorageService;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class FileController {


    private StorageService storageService;

    public FileController(StorageService storageService) {
        this.storageService = storageService;
    }

    @GetMapping("/student/list-files")
    public String listAllFilesStudent() {

        List<String> toPrint = storageService.loadAll().map(
                        path -> ServletUriComponentsBuilder.fromCurrentContextPath()
                                .path("/download/")
                                .path(path.getFileName().toString())
                                .toUriString())
                .collect(Collectors.toList());

        return toPrint.toString();
    }

    @GetMapping("/professor/list-files")
    public String listAllFilesProfessor() {

        List<String> toPrint = storageService.loadAll().map(
                        path -> ServletUriComponentsBuilder.fromCurrentContextPath()
                                .path("/download/")
                                .path(path.getFileName().toString())
                                .toUriString())
                .collect(Collectors.toList());

        return toPrint.toString();
    }



    @GetMapping("/student/download/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> downloadFileStudent(@PathVariable String filename) {

        Resource resource = storageService.loadAsResource(filename);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);

    }

    @GetMapping("/professor/download/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> downloadFileProfessor(@PathVariable String filename) {

        Resource resource = storageService.loadAsResource(filename);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);

    }

    @PostMapping("/student/upload-file")
    @ResponseBody
    public FileResponse uploadFileStudent(@RequestParam("file") MultipartFile file) {
        String name = storageService.store(file);

        String uri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/download/")
                .path(name)
                .toUriString();

        return new FileResponse(name, uri, file.getContentType(), file.getSize());
    }

    @PostMapping("/professor/upload-file")
    @ResponseBody
    public FileResponse uploadFileProfessor(@RequestParam("file") MultipartFile file) {
        String name = storageService.store(file);

        String uri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/download/")
                .path(name)
                .toUriString();

        return new FileResponse(name, uri, file.getContentType(), file.getSize());
    }

    @PostMapping("/upload-file")
    @ResponseBody
    public FileResponse uploadFile(@RequestParam("file") MultipartFile file) {
        String name = storageService.store(file);

        String uri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/download/")
                .path(name)
                .toUriString();

        return new FileResponse(name, uri, file.getContentType(), file.getSize());
    }
    @PostMapping("/student//upload-multiple-files")
    @ResponseBody
    public List<FileResponse> uploadMultipleFilesStudent(@RequestParam("files") MultipartFile[] files) {
        return Arrays.stream(files)
                .map(file -> uploadFileStudent(file))
                .collect(Collectors.toList());
    }

    @PostMapping("/professor/upload-multiple-files")
    @ResponseBody
    public List<FileResponse> uploadMultipleFilesProfessor(@RequestParam("files") MultipartFile[] files) {
        return Arrays.stream(files)
                .map(file -> uploadFileProfessor(file))
                .collect(Collectors.toList());
    }
}
