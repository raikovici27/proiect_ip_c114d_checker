package dev.c114d.api.Controllers;

import dev.c114d.api.Kafka.JsonKafkaProducer;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static dev.c114d.api.Controllers.StudentController.kafkaMessage;

@RestController
@RequestMapping("/api/v1/kafka")
public class JsonKafkaController {
    private JsonKafkaProducer kafkaProducer;

    public JsonKafkaController(JsonKafkaProducer kafkaProducer) {
        this.kafkaProducer = kafkaProducer;
    }

//    @PostMapping("/publishMail")
//    public ResponseEntity<String> publishMail(){
//        kafkaProducer.sendMessageMail(kafkaMessage);
//        return ResponseEntity.ok( kafkaMessage.toString());
//    }
//
//    @PostMapping("/publishGoogle")
//    public ResponseEntity<String> publishGoogle(){
//        kafkaProducer.sendMessageSheets(kafkaMessage);
//        return ResponseEntity.ok( kafkaMessage.toString());
//    }

    @PostMapping("/toExecutor")
    public ResponseEntity<String> sendMessageExecutor() {
        kafkaProducer.sendMessageExecutor(kafkaMessage);
        return ResponseEntity.ok( kafkaMessage.toString());
    }

    @GetMapping("/fromExecutor")
    public ResponseEntity<String> getMessageExecutor() {
        kafkaProducer.sendMessageExecutor(kafkaMessage);
        return ResponseEntity.ok( kafkaMessage.toString());
    }
}
