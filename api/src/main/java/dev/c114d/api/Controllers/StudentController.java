package dev.c114d.api.Controllers;

import dev.c114d.api.Handlers.DatabaseHandler;
import dev.c114d.api.Models.KafkaMessage;
import dev.c114d.api.Models.Users;
//import org.bouncycastle.cert.ocsp.Req;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.json.JSONObject;
import dev.c114d.api.Models.KafkaMessage;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

@RestController
@RequestMapping("/student")
public class StudentController {

    @Autowired
    private DatabaseHandler databaseHandler;
    public static KafkaMessage kafkaMessage;
    public static String json;
    @GetMapping(value = "/classesattended", params = {"idStudent"})
    ResponseEntity getClassesAttended(@RequestParam(value = "idStudent") Integer idStudent){
        return  new ResponseEntity<>(databaseHandler.getClassesAttended(idStudent), HttpStatus.OK);
    }

    @GetMapping(value = "/assignmentsforsubject", params = {"idStudent", "idSubject"})
    ResponseEntity getAssignmentsForSubject(@RequestParam(value = "idStudent") Integer idStudent,
                                            @RequestParam(value = "idSubject") Integer idSubject){
        return  new ResponseEntity<>(databaseHandler.getAssignmentsForSubject(idStudent, idSubject), HttpStatus.OK);
    }

    @PostMapping(value = "/addStudentUpload", params = {"idStudent", "idTeacherPost", "sourceCodeFilePath"})
    public ResponseEntity<String> addTeacherPost(@RequestParam(value = "idStudent") String idStudent,
                                                 @RequestParam(value = "idTeacherPost") String idTeacherPost,
                                                 @RequestParam(value = "sourceCodeFilePath") String sourceCodeFilePath) throws IOException {
        databaseHandler.addStudentUpload(idStudent, idTeacherPost, sourceCodeFilePath);

        Users u = databaseHandler.getUserById(Integer.parseInt(idStudent));
        String username = u.getUsername();
        String email = u.getMailAddress();
        String note = "0";
        JSONObject requestBody = new JSONObject();
        requestBody.put("username", username);
        requestBody.put("email", email);
        requestBody.put("teacherPost", idTeacherPost);
        requestBody.put("note", note);
        requestBody.put("data", sourceCodeFilePath);
        json = requestBody.toString();
        System.out.println(json);

        kafkaMessage = new KafkaMessage(json);
        kafkaMessage.deserialization();
        return ResponseEntity.ok(json);

    }


}
