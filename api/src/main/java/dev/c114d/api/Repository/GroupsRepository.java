package dev.c114d.api.Repository;

import dev.c114d.api.Models.Groups;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupsRepository extends JpaRepository<Groups, Integer> {

    @Query(value = "select * from Groups", nativeQuery = true)
    List<Object> getGroups();

    Groups findByGroupName(String groupName);
}
