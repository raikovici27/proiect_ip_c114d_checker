package dev.c114d.api.Repository;

import dev.c114d.api.Models.Students;
import dev.c114d.api.Models.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentsRepository extends JpaRepository<Students, Integer> {

    @Query(value = "select * from Students", nativeQuery = true)
    List<Object> getStudents();

    Students findByUser(Users user);
}