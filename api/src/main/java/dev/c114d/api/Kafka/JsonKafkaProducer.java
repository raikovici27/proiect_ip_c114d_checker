package dev.c114d.api.Kafka;

import dev.c114d.api.Models.KafkaMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
public class JsonKafkaProducer {

    private static final Logger LOGGER = LoggerFactory.getLogger(JsonKafkaProducer.class);

    private KafkaTemplate<String, KafkaMessage> kafkaTemplate;

    public JsonKafkaProducer(KafkaTemplate<String, KafkaMessage> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void sendMessageMail(KafkaMessage data){

        LOGGER.info(String.format("%s", data.toString()));

        org.springframework.messaging.Message<KafkaMessage> message = MessageBuilder
                .withPayload(data)
                .setHeader(KafkaHeaders.TOPIC, "mail")
                .build();

        kafkaTemplate.send(message);
    }

    public void sendMessageSheets(KafkaMessage data){

        LOGGER.info(String.format("%s", data.toString()));

        org.springframework.messaging.Message<KafkaMessage> message = MessageBuilder
                .withPayload(data)
                .setHeader(KafkaHeaders.TOPIC, "sheets")
                .build();

        kafkaTemplate.send(message);
    }

    public void sendMessageExecutor(KafkaMessage data){

        LOGGER.info(String.format("%s", data.toString()));

        org.springframework.messaging.Message<KafkaMessage> message = MessageBuilder
                .withPayload(data)
                .setHeader(KafkaHeaders.TOPIC, "executor")
                .build();

        kafkaTemplate.send(message);
    }
}