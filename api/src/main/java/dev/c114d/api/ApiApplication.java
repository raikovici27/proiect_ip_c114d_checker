package dev.c114d.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import dev.c114d.api.FileUploadDownloadLocal.storage.StorageProperties;

@SpringBootApplication

// We need this for file upload and download management, do not remove!
@EnableConfigurationProperties(StorageProperties.class)
public class ApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiApplication.class, args);
	}

}
