package dev.c114d.api.Handlers;


import dev.c114d.api.JSONTypes.AssignmentJSON;
import dev.c114d.api.JSONTypes.ClassAttendedJSON;
import dev.c114d.api.JSONTypes.ClassesTaughtJSON;
import dev.c114d.api.JSONTypes.ResultJSON;
import dev.c114d.api.Models.*;
import dev.c114d.api.Repository.*;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.*;

@Service
public class DatabaseHandler {
    @Autowired
    private ClassesRepository classesRepository;
    public List<Object> getAllClasses()
    {
        return classesRepository.getClasses();
    }
    public void assignClass(String usernameTeacher, String subjectName, String classTypeName){
        Users teacher = this.getUserByUsername(usernameTeacher);
        ClassTypes classType = this.getClassTypeByName(classTypeName);
        Subjects subjects = this.getBySubjectNameAndClassType(subjectName, classType);

        Classes newClass = new Classes();
        newClass.setUserTeacher(teacher);
        newClass.setSubject(subjects);
        classesRepository.save(newClass);
    }

    public Classes getClassBySubjectAndTeacher(Subjects subject, Users userTeacher) { return classesRepository.findBySubjectAndUserTeacher(subject, userTeacher); }
    @Autowired
    private ClassTypesRepository classTypesRepository;
    public List<Object> getAllClassTypes() { return classTypesRepository.getClassTypes(); }
    public ClassTypes getClassTypeByName(String classTypeName) { return classTypesRepository.findByClassTypeName(classTypeName); }

    @Autowired
    private GroupListsRepository groupListsRepository;
    public List<Object> getAllGroupLists() { return groupListsRepository.getGroupLists(); }
    public List<GroupLists> getGroupListByGroup(Groups group) { return groupListsRepository.findGroupListByGroup(group); }
    public void assignGroupToClass(String usernameTeacher, String subjectName, String classTypeName,
                               String groupName) {
        Users teacher = this.getUserByUsername(usernameTeacher);
        ClassTypes classType = this.getClassTypeByName(classTypeName);
        Subjects subject = this.getBySubjectNameAndClassType(subjectName, classType);
        Classes classByTeacher = this.getClassBySubjectAndTeacher(subject, teacher);
        Groups group = this.getGroupByName(groupName);

        GroupLists newGroupList = new GroupLists();
        newGroupList.setClasss(classByTeacher);
        newGroupList.setGroup(group);
        groupListsRepository.save(newGroupList);
    }

    @Autowired
    private GroupsRepository groupsRepository;
    public List<Object> getAllGroups() { return groupsRepository.getGroups(); }
    public void addGroup(String groupName){
        Groups newGroup = new Groups();
        newGroup.setGroupName(groupName);
        groupsRepository.save(newGroup);
    }
    public Groups getGroupByName(String groupName) { return groupsRepository.findByGroupName(groupName); }
    @Autowired
    private RolesRepository rolesRepository;
    public List<Object> getAllRoles()
    {
        return rolesRepository.getRoles();
    }
    public Roles getRoleIdByName(String roleName) { return rolesRepository.findByDescription(roleName); }

    @Autowired
    private SpecializationsRepository specializationsRepository;
    public List<Object> getAllSpecializations() { return specializationsRepository.getSpecializations(); }
    public void addSpecialization(String specializationName) {
        Specializations newSpecialization = new Specializations();
        newSpecialization.setSpecializationName(specializationName);
        specializationsRepository.save(newSpecialization);
    }
    public Specializations getSpecializationByName(String specializationName) {
        return specializationsRepository.findBySpecializationName(specializationName);
    }

    @Autowired
    private StudentsRepository studentsRepository;
    public List<Object> getAllStudents() { return studentsRepository.getStudents(); }

    public Students getStudentByUser(Users user) { return studentsRepository.findByUser(user); }

    @Autowired
    private StudentUploadRepository studentUploadRepository;
    public List<Object> getAllStudentUploads() { return studentUploadRepository.getStudentUploads(); }
    public void addStudentUpload(String idStudent, String idTeacherPost, String sourceCodeFilePath)
    {
        Users user = this.getUserById(Integer.parseInt(idStudent));
        Students student = this.getStudentByUser(user);
        TeacherPost teacherPost = this.getTeacherPostById(Integer.parseInt(idTeacherPost));

        StudentUpload newStudentUpload = new StudentUpload();
        newStudentUpload.setTimestamp(OffsetDateTime.now());
        newStudentUpload.setStudent(student);
        newStudentUpload.setTeacherPost(teacherPost);
        newStudentUpload.setSourceCodeFilePath(sourceCodeFilePath);
        studentUploadRepository.save(newStudentUpload);
    }

    @Autowired
    private SubjectsRepository subjectsRepository;
    public List<Object> getAllSubjects() { return subjectsRepository.getSubjects(); }
    public void addSubject(String subjectName, String classTypeName) {
        ClassTypes classType = this.getClassTypeByName(classTypeName);

        Subjects newSubject = new Subjects();
        newSubject.setSubjectName(subjectName);
        newSubject.setClassType(classType);
        subjectsRepository.save(newSubject);
    }
    public Subjects getBySubjectNameAndClassType(String subjectName, ClassTypes classType) { return subjectsRepository.findBySubjectNameAndClassType(subjectName, classType); }
    public Subjects getSubjectById(Integer id) { return subjectsRepository.findByIdSubject(id); }
    @Autowired
    private TeacherPostRepository teacherPostRepository;
    public List<Object> getAllTeacherPosts() { return teacherPostRepository.getTeacherPosts(); }
    public List<TeacherPost> getTeacherPostByClass(Classes givenClass) { return teacherPostRepository.findByClasss(givenClass); }
    public TeacherPost getTeacherPostById(Integer id) { return teacherPostRepository.findByIdTeacherPost(id); }
    public void addTeacherPost(String postName, String idSubject, String idTeacher, String startTime,
                               String endTime, String assignmentFilePath, String description){
        Users teacher = this.getUserById(Integer.parseInt(idTeacher));
        Subjects subject = this.getSubjectById(Integer.parseInt(idSubject));
        Classes classByTeacher = this.getClassBySubjectAndTeacher(subject, teacher);

        TeacherPost newTeacherPost = new TeacherPost();
        newTeacherPost.setClasss(classByTeacher);
        newTeacherPost.setPostName(postName);
        ZoneOffset offset = ZoneOffset.UTC;
        newTeacherPost.setStopTime(OffsetDateTime.of(LocalDateTime.parse(endTime.replace(' ', 'T')), offset));
        newTeacherPost.setStartTime(OffsetDateTime.of(LocalDateTime.parse(startTime.replace(' ', 'T')), offset));
        newTeacherPost.setTimestamp(OffsetDateTime.now());
        newTeacherPost.setAssignmentFilePath(assignmentFilePath);
        newTeacherPost.setAssignmentContent(description);
        teacherPostRepository.save(newTeacherPost);
    }
    @Autowired
    private UsersRepository usersRepository;
    public List<Object> getAllUsers()
    {
        return usersRepository.getUsers();
    }
    public Users getUserByUsername(String username) {return usersRepository.findByUsername(username); }
    public Users getUserById(Integer id) {return usersRepository.findByIdUser(id); }
    public void addAdmin(String surname,String firstname, String mailAddress, String username) {
        Roles role = this.getRoleIdByName("admin");

        Users newUser = new Users();
        newUser.setSurname(surname);
        newUser.setFirstname(firstname);
        newUser.setMailAddress(mailAddress);
        newUser.setUsername(username);
        newUser.setRole(role);
        usersRepository.save(newUser);
    }

    public void addProfesor(String surname,String firstname, String mailAddress, String username) {
        Roles role = this.getRoleIdByName("profesor");

        Users newUser = new Users();
        newUser.setSurname(surname);
        newUser.setFirstname(firstname);
        newUser.setMailAddress(mailAddress);
        newUser.setUsername(username);
        newUser.setRole(role);
        usersRepository.save(newUser);
    }
    public void addStudent(String surname,String firstname, String mailAddress, String username, String groupName,
                           String specializationName) {
        Roles role = this.getRoleIdByName("student");

        Users newUser = new Users();
        newUser.setSurname(surname);
        newUser.setFirstname(firstname);
        newUser.setMailAddress(mailAddress);
        newUser.setUsername(username);
        newUser.setRole(role);
        usersRepository.save(newUser);

        Users user = this.getUserByUsername(username);
        Specializations specialization = this.getSpecializationByName(specializationName);
        Groups group = this.getGroupByName(groupName);

        Students newStudent = new Students();
        newStudent.setGroup(group);
        newStudent.setSpecialization(specialization);
        newStudent.setUser(user);
        studentsRepository.save(newStudent);
    }

    public List<ClassAttendedJSON> getClassesAttended(Integer idStudent) {
        Users user = this.getUserById(idStudent);
        Students student = this.getStudentByUser(user);
        Groups studentGroup = student.getGroup();
        Set<GroupLists> groupLists = studentGroup.getGroupGroupListss();
        List<Classes> classes = new ArrayList<Classes>();
        for (GroupLists current : groupLists)
        {
            classes.add(current.getClasss());
        }
        List<Subjects> subjects = new ArrayList<Subjects>();
        for (Classes current : classes)
        {
            subjects.add(current.getSubject());
        }
        List<ClassAttendedJSON> returned= new ArrayList<ClassAttendedJSON>();
        for (Subjects current : subjects)
        {
            ClassAttendedJSON new_json = new ClassAttendedJSON();
            new_json.setIdSubject(current.getIdSubject());
            new_json.setSubjectName(current.getSubjectName());
            new_json.setClassTypeName(current.getClassType().getClassTypeName());
            returned.add(new_json);
        }
        return returned;
    }

    public List<AssignmentJSON> getAssignmentsForSubject(Integer idStudent, Integer idSubject) {
        Users user = this.getUserById(idStudent);
        Students student = this.getStudentByUser(user);
        Groups studentGroup = student.getGroup();
        Set<GroupLists> groupLists = studentGroup.getGroupGroupListss();
        List<Classes> classes = new ArrayList<Classes>();
        for (GroupLists current : groupLists)
        {
            if(current.getClasss().getSubject().getIdSubject().equals(idSubject))
                classes.add(current.getClasss());
        }
        List<TeacherPost> teacherPosts = new ArrayList<TeacherPost>();
        for (Classes current : classes)
        {
            teacherPosts.addAll(this.getTeacherPostByClass(current));
        }
        List<AssignmentJSON> returned= new ArrayList<AssignmentJSON>();
        for (TeacherPost current : teacherPosts)
        {
            AssignmentJSON new_json = new AssignmentJSON();
            new_json.setAssignmentContent(current.getAssignmentContent());
            new_json.setAssignmentFilePath(current.getAssignmentFilePath());
            new_json.setStopTime(current.getStopTime());
            new_json.setStartTime(current.getStartTime());
            new_json.setPostName(current.getPostName());
            new_json.setIdTeacherPost(current.getIdTeacherPost());
            returned.add(new_json);
        }
        return returned;
    }

    public List<ClassesTaughtJSON> getClassesTaught(Integer idProfessor)
    {
        Users user = this.getUserById(idProfessor);
        Set<Classes> classesTaught = user.getUserTeacherClassess();
        List<ClassesTaughtJSON> returned= new ArrayList<ClassesTaughtJSON>();
        for (Classes current : classesTaught)
        {
            ClassesTaughtJSON new_json = new ClassesTaughtJSON();
            new_json.setClassTypeName(current.getSubject().getClassType().getClassTypeName());
            new_json.setSubjectName(current.getSubject().getSubjectName());
            new_json.setIdSubject(current.getSubject().getIdSubject());
            returned.add(new_json);
        }
        return returned;
    }

    public List<AssignmentJSON> getAssignmentsGivenForSubject(Integer idProfessor, Integer idSubject)
    {
        Users user = this.getUserById(idProfessor);
        Set<Classes> classesTaught = user.getUserTeacherClassess();
        List<AssignmentJSON> returned= new ArrayList<AssignmentJSON>();

        for(Classes current: classesTaught) {
            if (current.getSubject().getIdSubject().equals(idSubject)) {
                Set<TeacherPost> teacherPosts = current.getClasssTeacherPosts();
                for (TeacherPost currentT : teacherPosts) {
                    AssignmentJSON new_json = new AssignmentJSON();
                    new_json.setAssignmentContent(currentT.getAssignmentContent());
                    new_json.setAssignmentFilePath(currentT.getAssignmentFilePath());
                    new_json.setStopTime(currentT.getStopTime());
                    new_json.setStartTime(currentT.getStartTime());
                    new_json.setPostName(currentT.getPostName());
                    new_json.setIdTeacherPost(currentT.getIdTeacherPost());
                    returned.add(new_json);
                }
            }
        }

        return returned;
    }

    public Integer getUserId(String username)
    {
        return usersRepository.findByUsername(username).getIdUser();
    }

    public List<ResultJSON> getResultsForTeacherPost(Integer idTeacherPost)
    {
        List<ResultJSON> returned= new ArrayList<ResultJSON>();

        TeacherPost teacherPost = teacherPostRepository.findById(idTeacherPost).get();

        Set<StudentUpload> studentUploads = teacherPost.getTeacherPostStudentUploads();
        List<StudentUpload> studentUploadsUnique = new ArrayList<StudentUpload>();
        for(StudentUpload current : studentUploads)
        {
            boolean found = false;
            for(StudentUpload currentUnique : studentUploadsUnique)
            {
                if(current.getStudent().getIdStudent().equals(currentUnique.getStudent().getIdStudent()))
                {
                    found = true;
                    if(current.getTimestamp().isAfter(currentUnique.getTimestamp()))
                    {
                        studentUploadsUnique.remove(currentUnique);
                        studentUploadsUnique.add(current);
                        break;
                    }
                }
            }
            if(found == false)
            {
                studentUploadsUnique.add(current);
            }
        }

        for(StudentUpload current : studentUploadsUnique)
        {
            Students student = current.getStudent();
            Groups group = student.getGroup();
            Users user = student.getUser();

            ResultJSON new_json = new ResultJSON();
            new_json.setIdStudentUpload(current.getIdStudentUpload());
            new_json.setPostName(teacherPost.getPostName());
            new_json.setUsername(user.getUsername());
            new_json.setFirstname(user.getFirstname());
            new_json.setSurname(user.getSurname());
            new_json.setGroupName(group.getGroupName());
            new_json.setResultsFilePath(current.getResultsFilePath());
            new_json.setResult(current.getResult());
            new_json.setTimestamp(current.getTimestamp());
            returned.add(new_json);
        }

        return returned;
    }
}
