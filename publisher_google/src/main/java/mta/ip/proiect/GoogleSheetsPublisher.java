package mta.ip.proiect;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.Value;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.*;
import com.sun.tools.javac.Main;

import java.io.*;
import java.security.GeneralSecurityException;
import java.util.*;

/* class to demonstarte use of Docs get documents API */
public class GoogleSheetsPublisher {
    private static Sheets sheetsService;
    private static String APPLICATION_NAME = "Google Sheets IP";
    private static String SPREADSHEET_ID = "1Rkjo7HistphZDMWdxOYZAr-DDQq2qJQaZZYf_xBlsN8"; //1Rkjo7HistphZDMWdxOYZAr-DDQq2qJQaZZYf_xBlsN8

    private static String Credentials_String = "/home/andrei/Desktop/publisher_google/src/main/resources/credentials.json";

    private static String Sheet_Name ="proiect";

    public GoogleSheetsPublisher(String Credentials_File_Path, String GOOGLE_SPREADSHEET_ID, String Sheet_Page_Name) throws GeneralSecurityException, IOException {
        SPREADSHEET_ID = GOOGLE_SPREADSHEET_ID;
        Credentials_String = Credentials_File_Path;
        Sheet_Name=Sheet_Page_Name;
        sheetsService = getSheetsService();
    }

    private static Credential authorize(String Credentials_File_Path) throws IOException, GeneralSecurityException {
        //InputStream in = SheetsQuickstart.class.getResourceAsStream("C:\\Users\\denis\\IdeaProjects\\publisher_google\\src\\main\\resources\\credentials.json");
        InputStream in =  new FileInputStream(Credentials_File_Path);
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(
               GsonFactory.getDefaultInstance(), new InputStreamReader(in)
        );

        List<String> scopes = Arrays.asList(SheetsScopes.SPREADSHEETS);

        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(GoogleNetHttpTransport.newTrustedTransport(),GsonFactory.getDefaultInstance(),clientSecrets,scopes)
                .setDataStoreFactory(new FileDataStoreFactory(new java.io.File("tokens")))
                .setAccessType("offline")
                .build();
        Credential credential = new AuthorizationCodeInstalledApp(
                flow,new LocalServerReceiver())
                .authorize("user");

        return credential;
    }

    public static Sheets getSheetsService() throws IOException, GeneralSecurityException {
        Credential credential = authorize(Credentials_String);
        return new Sheets.Builder(GoogleNetHttpTransport.newTrustedTransport(),
                GsonFactory.getDefaultInstance(),credential)
                .setApplicationName(APPLICATION_NAME)
                .build();
    }

    //=============READ DATA===================
    public List<List<Object>> Read_Data(String Sheets_Coord) throws IOException //Sheets_Coord ex: "proiect!A1:D2"
    {
        String range = Sheets_Coord; // = "proiect!A1:D2";

        ValueRange response = sheetsService.spreadsheets().values().get(SPREADSHEET_ID,range).execute();

        List<List<Object>> values = response.getValues();

        return values;

    }//=============END OF READ DATA===================
    //=============CREATE DATA===================
    public void Create_Data(String Surname,String Firstname, String Results) throws IOException {

        String range = "proiect!A1:D100"; // = "proiect!A1:D2";

        ValueRange response = sheetsService.spreadsheets().values().get(SPREADSHEET_ID,range).execute();

        List<List<Object>> values = response.getValues();

        if (values == null || values.isEmpty())
        {
            //System.out.println("No data found.");
            ValueRange appendBody = new ValueRange()
              .setValues(Arrays.asList(
                        Arrays.asList(Surname, Firstname,"", Results) //se lasa spatiu
             ));
            AppendValuesResponse appendResult = sheetsService.spreadsheets().values()
                    .append(SPREADSHEET_ID,"proiect",appendBody)
                    .setValueInputOption("USER_ENTERED")
                    .setInsertDataOption("INSERT_ROWS")
                    .setIncludeValuesInResponse(true)
                    .execute();
        }else {
//            for (List row : values)
//            {
//                System.out.printf("%s %s has got %s\n", row.get(1),row.get(0),row.get(3));
//            }
        }
        Integer coords;
        if(values==null)
        {
            coords = 1;
        }
        else
           coords=values.size() +1;

        ValueRange body1 = new ValueRange()
                .setValues(Arrays.asList(
                        Arrays.asList(Surname)
                ));
        UpdateValuesResponse result1 = sheetsService.spreadsheets().values()
                .update(SPREADSHEET_ID,"A" + coords.toString(),body1)
                .setValueInputOption("RAW")
                .execute();

        ValueRange body2 = new ValueRange()
                .setValues(Arrays.asList(
                        Arrays.asList(Firstname)
                ));
        UpdateValuesResponse result2 = sheetsService.spreadsheets().values()
                .update(SPREADSHEET_ID,"B" + coords.toString(),body2)
                .setValueInputOption("RAW")
                .execute();

        ValueRange body3 = new ValueRange()
                .setValues(Arrays.asList(
                        Arrays.asList(Results)
                ));
        UpdateValuesResponse result3 = sheetsService.spreadsheets().values()
                .update(SPREADSHEET_ID,"D" + coords.toString(),body3)
                .setValueInputOption("RAW")
                .execute();

    }
    //=============END OF CREATE DATA====================

    //=============UPDATE DATA====================
    public void Update_Data(String Updated_Value, String Sheets_Coord) throws IOException { //Sheets_Coord ex: A4
        ValueRange body11 = new ValueRange()
                .setValues(Arrays.asList(
                        Arrays.asList(Updated_Value)
                ));
        UpdateValuesResponse result11 = sheetsService.spreadsheets().values()
                .update(SPREADSHEET_ID,Sheets_Coord,body11)
                .setValueInputOption("RAW")
                .execute();

    }
    //=============END OF UPDATE DATA====================

    public void Delete_Data(int start_row_index) throws IOException { //start_row_index starts at 0 so +1, ex: 6
        //=============DELETE DATA====================
        DeleteDimensionRequest deleteRequest = new DeleteDimensionRequest()
                .setRange(
                        new DimensionRange().setSheetId(0)
                                .setDimension("ROWS")
                                .setStartIndex(start_row_index) //starts at 0 so +1
                );
        List<Request> requests = new ArrayList<>();
        requests.add(new Request().setDeleteDimension(deleteRequest));

        BatchUpdateSpreadsheetRequest body_del = new BatchUpdateSpreadsheetRequest().setRequests(requests);
        sheetsService.spreadsheets().batchUpdate(SPREADSHEET_ID,body_del).execute();

        //=============END OF DELETE DATA====================

    }
    public void Print_Data(List<List<Object>> values)
    {
    if(values!=null)
        for (List row : values)
        {
            System.out.printf("%s %s has got %s\n", row.get(1),row.get(0),row.get(3));
        }
    else
        System.out.println("No values were found!");
    }

}