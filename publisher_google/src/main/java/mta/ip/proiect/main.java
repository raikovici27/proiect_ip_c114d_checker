package mta.ip.proiect;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.util.List;



public class main {

    public static void main(String[] args) throws IOException, GeneralSecurityException, SQLException {

        {

            //Google_Publisher.Delete_Data(6);

            System.out.println("Attempting connection");

            String endpoint = "http://localhost:8090/api/v1/kafka/fromExecutor";

            HttpURLConnection connection = (HttpURLConnection) new URL(endpoint).openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            // Print the response
            System.out.println(response.toString());
            String resp = response.toString();
            resp = resp.substring(1, resp.length() - 1);

            String[] keyVals = resp.split(", ");

            String username = new String();
            String email = new String();
            String nota = new String();
            String date = new String();
            String idTeacherPost = new String();
            for (String keyVal : keyVals) {
                String[] parts = keyVal.split(":");

                String key = parts[0].substring(1, parts[0].length() - 1);
                String value = parts[1].substring(1, parts[1].length() - 1);

                if(key.equals("note"))
                    nota = value;
                else if(key.equals("teacherPost"))
                    idTeacherPost = value;
                else if(key.equals("username"))
                    username = value;
                else if(key.equals("email"))
                    email = value;
                else if(key.equals("data"))
                    date = value;
                System.out.println(key + " " + value);
            }
            String Creds = "/home/andrei/Desktop/publisheri/publisher_google/src/main/resources/credentials.json";
            String SheetID = "1Rkjo7HistphZDMWdxOYZAr-DDQq2qJQaZZYf_xBlsN8";
            String SheetName = "proiect";

            GoogleSheetsPublisher Google_Publisher = new GoogleSheetsPublisher(Creds,SheetID,SheetName);
            Google_Publisher.Create_Data(username,email,nota);

            List<List<Object>> data = Google_Publisher.Read_Data("proiect!A1:D4");
            Google_Publisher.Print_Data(data);
//
//
//            Google_Publisher.Create_Data(username,email,nota);
//            //Google_Publisher.Update_Data();
//            data = Google_Publisher.Read_Data("proiect!A1:D3");
//            Google_Publisher.Print_Data(data);
        }
    }
}
